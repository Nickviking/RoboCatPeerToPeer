﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerControllerGato : MonoBehaviour
{
    public MyCatScript connManager;
    public PlayerGun playerGun;
    public Rigidbody2D myRigidBody;
    public Vector3 lastPosition;
    public float lastAngle;
    public int life = 3;
    public Vector3 screenSize;
    // Use this for initialization
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        playerGun = GetComponent<PlayerGun>();
        screenSize = Camera.main.ScreenToWorldPoint(transform.position);
        //connManager.SendMyPosition(transform.position.x, transform.position.y, transform.eulerAngles.z);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        myRigidBody.velocity = transform.right * Input.GetAxis("Vertical") * 2f;
        transform.Rotate(0, 0, Input.GetAxis("Horizontal") * 2f);

        if (transform.position.x < screenSize.x || transform.position.x > -screenSize.x)
        {
            transform.position = new Vector3(transform.position.x * -1, transform.position.y, transform.position.z);
        }
        if (transform.position.y < screenSize.y || transform.position.y > -screenSize.y)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y * -1, transform.position.z);
        }



        if (lastPosition != transform.position || lastAngle != transform.eulerAngles.z)
        {
            connManager.SendMyPosition(transform.position.x, transform.position.y, transform.eulerAngles.z);
            lastPosition = transform.position;
            lastAngle = transform.eulerAngles.z;
        }

    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            //connManager.Shoot();
            playerGun.Shoot();
        }
    }

    public void Shooted()
    {
        life--;
        if(life <= 0)
        {
            //connManager.Disconnect();
            SceneManager.LoadScene("ModeSelect");
        }
    }
}

﻿using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;

public class MyCatScript : MonoBehaviour
{
    private DebugScript debug;
    private const int MAX_CONNECTION = 100;
    [SerializeField]
    private int port = 8000;
    [SerializeField]
    private int hostId = -1;
    private int reliableChannel;
    private int unreliableChannel;
    [SerializeField]
    private bool isStarted = false;
    private byte error;
    public List<AnotherCat> anotherCats;

    public AnotherCats catsController;

    public void Start()
    {
        debug = GameObject.Find("Debug").GetComponent<DebugScript>();
        catsController = GameObject.Find("CatsController").GetComponent<AnotherCats>();
        anotherCats = new List<AnotherCat>();
    }

    public void StartServer(int port)
    {
        debug.AddLog("Starting Server");
        NetworkTransport.Init();
        ConnectionConfig cc = new ConnectionConfig();

        reliableChannel = cc.AddChannel(QosType.Reliable);
        unreliableChannel = cc.AddChannel(QosType.Unreliable);

        HostTopology topo = new HostTopology(cc, MAX_CONNECTION);
        this.port = port;
        hostId = NetworkTransport.AddHost(topo, port, null);
        if (hostId >= 0)
        {
            isStarted = true;
            debug.AddLog("Server Started at port: " + port);
        }
    }

    private void Update()
    {
        if (!isStarted)
            return;
        int recHostId;
        int connectionId;
        int channelId;
        byte[] recBuffer = new byte[1024];
        int bufferSize = 1024;
        int dataSize;
        byte error;
        NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
        switch (recData)
        {
            case NetworkEventType.ConnectEvent:
                OnConnection(connectionId);
                break;
            case NetworkEventType.DataEvent:
                string msg = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
                string[] splitData = msg.Split('|');
                switch (splitData[0])
                {
                    case "IPIS":
                        OnIPIs(connectionId, splitData[1],int.Parse(splitData[2]));
                        break;
                }
                break;
            case NetworkEventType.DisconnectEvent:
                OnDisconnection(connectionId);
                break;
        }
    }

    public int GetPort()
    {
        return port;
    }

    private void OnConnection(int connId)
    {
        debug.AddLog("ASKIP");
        Send("ASKIP|" + Network.player.ipAddress, reliableChannel, connId);
    }

    public void SendMyPosition(float x, float y, float angle)
    {
        print(anotherCats.Count);
        string msg = "MYPOSITION|" + x + "|" + y + "|" + angle;
        Send(msg, unreliableChannel, anotherCats);
    }

    private void OnIPIs(int connId, string playerIP, int catPort)
    {

        AnotherCat c = new AnotherCat();
        c.conId = connId;
        c.ipAdrees = playerIP;
        c.port = catPort;
        anotherCats.Add(c);
        StartCoroutine(SpawnAnotherCat(playerIP, catPort));
        debug.AddLog("established connection with " + connId.ToString() + " " + playerIP);

    }

    IEnumerator SpawnAnotherCat(string playerIP,int port)
    {
        yield return new WaitForSeconds(5f);
        catsController.SpawnAnotherCat(playerIP, port);
    }

    private void OnDisconnection(int connId)
    {
        string ip = "NAO ACHOU!!!!";
        for (int i = 0; i < anotherCats.Count; i++)
        {
            if (anotherCats[i].conId == connId)
            {
                ip = anotherCats[i].ipAdrees;
            }
        }
        catsController.DestroyCat(ip);
        debug.AddLog(ip + " Removed");
        anotherCats.Remove(anotherCats.Find(x => x.conId == connId));
    }

    private void Send(string message, int channelId, int connId)
    {
        byte[] msg = Encoding.Unicode.GetBytes(message);
        NetworkTransport.Send(hostId, connId, channelId, msg, message.Length * sizeof(char), out error);
    }

    private void Send(string message, int channelId, List<AnotherCat> c, int connId)
    {
        List<AnotherCat> clientlist = new List<AnotherCat>();
        foreach (AnotherCat item in anotherCats)
        {
            clientlist.Add(item);
        }
        clientlist.Remove(clientlist.Find(x => x.conId == connId));
        Send(message, channelId, clientlist);
    }

    private void Send(string message, int channelId, List<AnotherCat> c)
    {
        byte[] msg = Encoding.Unicode.GetBytes(message);
        foreach (AnotherCat client in c)
        {
            NetworkTransport.Send(hostId, client.conId, channelId, msg, message.Length * sizeof(char), out error);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGun : MonoBehaviour
{
    public GameObject shotPrefab;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Shoot()
    {
        GameObject shot = Instantiate(shotPrefab, transform.position, transform.rotation);
        shot.GetComponent<Rigidbody2D>().AddForce(shot.transform.right * 500f);
    }

}

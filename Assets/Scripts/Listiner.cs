﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;



public class Listiner : NetworkDiscovery
{
    public Broadcaster broadcaster;
    public MyCatScript myCat;
    public DebugScript debug;
    public bool isStarted;
    public int port;

    public AnotherCats catsController;

    public List<string> anotherCats;

    // Use this for initialization
    void Start()
    {
        anotherCats = new List<string>();

        catsController = GameObject.Find("CatsController").GetComponent<AnotherCats>();
        debug = GameObject.Find("Debug").GetComponent<DebugScript>();
        myCat = GameObject.Find("MyCat").GetComponent<MyCatScript>();
        broadcaster = GameObject.Find("BroadcasterSender").GetComponent<Broadcaster>();

        debug.AddLog("My ip is: " + Network.player.ipAddress);

        port = 8000;

        Initialize();
        isStarted = StartAsClient();
        if (isStarted)
        {
            debug.AddLog("Listening port " + broadcastPort);
            StartCoroutine(CreateMyCat());
        }
    }

    IEnumerator CreateMyCat()
    {

        yield return new WaitForSeconds(5f);
        myCat.StartServer(port);
        broadcaster.StartBroadcast(port.ToString());

    }

    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        string[] address = fromAddress.Split(':');

        print(fromAddress + " " + data);

        if (address[3] != Network.player.ipAddress && !anotherCats.Contains(address[3]))
        {
            port = int.Parse(data) + 1;
            debug.AddLog("Another Cat Finded: " + address[3] + ":" + data);

            anotherCats.Add(address[3]);
            //catsController.SpawnAnotherCat(address[3], int.Parse(data));
        }
    }



}
﻿using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text;
using System.Collections.Generic;

public class OtherCatsScript : MonoBehaviour
{
    private const int MAX_CONNECTION = 100;
    private int port;
    private int hostId;
    private int reliableChannel;
    private int unreliableChannel;

    private int ourClientId;
    private int connectionId;
    private string playerIP;

    public float connectionTime;
    private bool isConnected = false;
    private bool isStarted = false;

    private byte error;

    public DebugScript debug;


    public void Connect(string ip, int catPort)
    {
        debug = GameObject.Find("Debug").GetComponent<DebugScript>();
        NetworkTransport.Init();
        ConnectionConfig cc = new ConnectionConfig();

        reliableChannel = cc.AddChannel(QosType.Reliable);
        unreliableChannel = cc.AddChannel(QosType.Unreliable);

        HostTopology topo = new HostTopology(cc, MAX_CONNECTION);

        hostId = NetworkTransport.AddHost(topo, 0);

        connectionId = NetworkTransport.Connect(hostId, ip, catPort, 0, out error);

        isConnected = true;

        debug.AddLog("Cat Connected - ConnectionID: " + connectionId.ToString() + " IP: " + ip + " Port: " + catPort);

    }

    private void Update()
    {
        if (!isConnected)
            return;

        int recHostId;
        int connectionId;
        int channelId;
        byte[] recBuffer = new byte[1024];
        int bufferSize = 1024;
        int dataSize;
        byte error;
        NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
        switch (recData)
        {

            case NetworkEventType.DataEvent:
                string msg = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
                string[] splitData = msg.Split('|');

                switch (splitData[0])
                {
                    case "ASKIP":
                        OnAskIP();
                        break;
                    case "MYPOSITION":
                        OnUpdatePosition(splitData);
                        break;
                    case "SHOT":
                        // OnShot(int.Parse(splitData[1]));
                        break;
                    default:
                        break;
                }
                break;
        }
    }
    private void OnAskIP()
    {
        debug.AddLog("IP SENDED");
        Send("IPIS|" + Network.player.ipAddress + "|" + port, reliableChannel);
    }

    public void Shoot()
    {
        //  string message = "MYSHOOT";
        //   Send(message, reliableChannel);
    }

    private void OnUpdatePosition(string[] data)
    {
        Vector2 position = Vector2.zero;
        Quaternion rotation = new Quaternion();

        position.x = float.Parse(data[1]);
        position.y = float.Parse(data[2]);
        rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, float.Parse(data[3]));

        transform.position = position;
        transform.rotation = rotation;
    }

    private void OnShot(int connId)
    {
        //  if (!isStarted)
        //   {
        //      return;
        //  }
        // players[connId].avatar.GetComponent<PlayerGun>().Shoot();
    }



    private void Send(string message, int channelId)
    {
        byte[] msg = Encoding.Unicode.GetBytes(message);
        NetworkTransport.Send(hostId, connectionId, channelId, msg, message.Length * sizeof(char), out error);
    }

}
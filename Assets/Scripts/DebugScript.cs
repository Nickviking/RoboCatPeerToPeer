﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugScript : MonoBehaviour
{
    public Transform content;
    public Scrollbar scroll;
    // Use this for initialization
    void Start()
    {
        content = GameObject.Find("DebugContent").GetComponent<Transform>();
        scroll = GameObject.Find("DebugScrollBar").GetComponent<Scrollbar>();
        AddLog("LogTest");
    }

    public void AddLog(string text)
    {
        GameObject log = new GameObject("log");
        log.transform.SetParent(content,false);
        log.AddComponent<RectTransform>();
        log.AddComponent<CanvasRenderer>();
        Text logText = log.AddComponent<Text>();
        logText.text = text;
        logText.font = Font.CreateDynamicFontFromOSFont("Arial", 256);
        StartCoroutine(scrollDown());
       // Instantiate(log, content);
    }

    public IEnumerator scrollDown()
    {
        yield return new WaitForEndOfFrame();
        scroll.value = 0;
    }
}

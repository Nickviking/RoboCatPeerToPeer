﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnotherCat
{
    public int conId;
    public string ipAdrees;
    public int port;
    public GameObject avatar;
}

public class AnotherCats : MonoBehaviour
{

    public List<AnotherCat> cats = new List<AnotherCat>();
    public GameObject anotherCatPrefab;
    public DebugScript debug;

    private void Start()
    {
        debug = GameObject.Find("Debug").GetComponent<DebugScript>();
    }

    public void SpawnAnotherCat(string ip, int port)
    {
        AnotherCat cat = new AnotherCat();
        cat.ipAdrees = ip;
        cat.port = port;
        debug.AddLog("Instantiating another cat");
        cat.avatar = Instantiate(anotherCatPrefab);
        cat.avatar.GetComponent<OtherCatsScript>().Connect(ip, port);
        cats.Add(cat);
    }

    public void DestroyCat(string ip)
    {
        Destroy(cats.Find(x => x.ipAdrees == ip).avatar);
        cats.Remove(cats.Find(x => x.ipAdrees == ip));
    }

}

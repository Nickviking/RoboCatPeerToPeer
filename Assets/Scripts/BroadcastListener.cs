﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class BroadcastListener : NetworkDiscovery {
    public DebugScript debug;
	// Use this for initialization
	void Start () {
        debug = GameObject.Find("Debug").GetComponent<DebugScript>();
        Initialize();
        StartAsClient();
	}
	
    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        base.OnReceivedBroadcast(fromAddress, data);

        debug.AddLog(fromAddress + " " + data);
    }
}

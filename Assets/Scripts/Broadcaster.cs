﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Broadcaster : NetworkDiscovery
{
    public DebugScript debug;
    public bool isStarted;
    void Start()
    {
        debug = GameObject.Find("Debug").GetComponent<DebugScript>();

    }

    public void StartBroadcast(string data)
    {
        broadcastData = data;
        Initialize();
        isStarted = StartAsServer();    
        if (isStarted)
        {
            debug.AddLog("Broadcasting port " + broadcastPort);
        }
    }
}
